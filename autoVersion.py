'''
    File name: autoVersion.py
    Author: roy
    Date created: 2016-07-18
    Date last modified: 2016-07-18
    Python Version: 2.7

    Description:

        aktualisiert die version der lokalen pom datei, indem die remote/master pom.xml mit einer get request abgefragt wird. 

        format  :YY.MM.X [\d]{2}\.[\d]{1,2}\.[\d]+

        YY     : Jahr 2016 => 16
        MM     : Monat Mai => 5 | Dezember => 12
        X      : Diese zahl wird bei der versionierung hochgezaehlt

    *   Git hook (pre-push):

            #!/bin/bash

            currentBranch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

            if [ "$currentBranch" = "master" ]
            then
              newVersion=$(python .git/myScripts/autoVersion.py)
              
              case $newVersion in
                  "[ERROR]"* ) 
                echo -e "\n[ERROR] Versionierung fehlgeschlagen, Push abgebrochen, repository wird auf den letzten commit zurueckgesetzt\n"
                echo "$newVersion"
                git reset --hard 
                exit 1;;
              esac
              
              git commit pom.xml -m"[Version: $newVersion]"
              git push --no-verify origin master
              echo "\n[NEW VERSION] $newVersion\n"
            fi
            exit 0
'''

import re
import datetime
import urllib
import tempfile
from xml.etree.ElementTree import parse
import xml.etree.ElementTree as ET

today = datetime.date.today()
urlToRemoteMasterPom = "http://192.168.1.49:3000/administrator/auto-version/raw/master/pom.xml"

temppom = str(tempfile.mkstemp(dir=tempfile.gettempdir())[1])

def getNewVersionByRemoteMaster():
    try:
        incrementor = 0
        url = urllib.urlopen(urlToRemoteMasterPom)

        content = url.read()
        f = open(temppom, "w")
        f.write(content)
        f.close()

        doc = parse(temppom)

        masterVersion = doc.find("version").text
        versionSplit = re.split("\.", masterVersion)

        if str(re.sub("^0", "", today.strftime('%m'))) != str(versionSplit[1]) or str(today.strftime('%Y')[2:4]) != str(versionSplit[0]):
            incrementor += 1
        else:
            incrementor = int(versionSplit[len(versionSplit) - 1]) +1
                  
        if incrementor != 0:
            return today.strftime('%Y')[2:4] + "." + re.sub("^0", "", today.strftime('%m')) + "." + str(incrementor)

    except Exception, e:
        return str(e)

def updatePom(newVersion):
    doc = parse("pom.xml")
    doc.find("version").text = newVersion
    doc.write("pom.xml", "utf-8")

    return newVersion

def main():
    versionFromMaster = getNewVersionByRemoteMaster()

    if (re.match("[\d]{2}\.[\d]{1,2}\.[\d]+", versionFromMaster)):
        print updatePom(versionFromMaster)
    else:
        print "[ERROR] " + versionFromMaster

if __name__ == "__main__":
    main()
